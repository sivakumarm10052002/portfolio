document.addEventListener('DOMContentLoaded', () => {
    const menuBtn = document.querySelector('.menu-btn');
    const navLinks = document.querySelector('.nav-links');

    menuBtn.addEventListener('click', () => {
        navLinks.style.display = navLinks.style.display === 'flex' ? 'none' : 'flex';
    });

    // Smooth scrolling for navigation links
    document.querySelectorAll('a[href^="#"]').forEach(anchor => {
        anchor.addEventListener('click', function (e) {
            e.preventDefault();
            document.querySelector(this.getAttribute('href')).scrollIntoView({
                behavior: 'smooth'
            });
        });
    });

    // Custom cursor
    const cursor = document.querySelector('.custom-cursor');
    
    document.addEventListener('mousemove', (e) => {
        cursor.style.left = e.clientX + 'px';
        cursor.style.top = e.clientY + 'px';
    });

    document.addEventListener('mousedown', () => {
        cursor.style.transform = 'scale(0.8)';
    });

    document.addEventListener('mouseup', () => {
        cursor.style.transform = 'scale(1)';
    });

    // Typing Animation
    const roles = ['Software Engineer', 'Backend Developer', 'DevOps Engineer'];
    const typingText = document.querySelector('.typing-text');
    let roleIndex = 0;
    let charIndex = 0;
    let isDeleting = false;
    let isWaiting = false;

    function typeEffect() {
        const currentRole = roles[roleIndex];
        const shouldDelete = isDeleting && charIndex > 0;
        const shouldWrite = !isDeleting && charIndex < currentRole.length;

        if (shouldWrite) {
            typingText.textContent = currentRole.substring(0, charIndex + 1);
            charIndex++;
        } else if (shouldDelete) {
            typingText.textContent = currentRole.substring(0, charIndex - 1);
            charIndex--;
        }

        // Handle role switching
        if (!isDeleting && charIndex === currentRole.length) {
            isWaiting = true;
            setTimeout(() => {
                isDeleting = true;
                isWaiting = false;
            }, 2000); // Wait time at end of word
        } else if (isDeleting && charIndex === 0) {
            isDeleting = false;
            roleIndex = (roleIndex + 1) % roles.length;
        }

        // Set typing speed
        let typeSpeed = isDeleting ? 50 : 100;
        if (isWaiting) typeSpeed = 2000;

        setTimeout(typeEffect, typeSpeed);
    }

    // Start the typing animation
    typeEffect();

    // Add scroll reveal animation
    const revealElements = document.querySelectorAll('.skill-category, .project-card');
    
    const observer = new IntersectionObserver((entries) => {
        entries.forEach(entry => {
            if (entry.isIntersecting) {
                entry.target.style.opacity = '1';
                entry.target.style.transform = 'translateY(0)';
            }
        });
    });

    revealElements.forEach(element => {
        element.style.opacity = '0';
        element.style.transform = 'translateY(20px)';
        element.style.transition = 'all 0.6s ease';
        observer.observe(element);
    });
}); 